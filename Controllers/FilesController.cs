﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BoxDrop.Models;
using Microsoft.AspNetCore.Hosting;
using BoxDrop.Controllers.API.UniqueKey;
using Microsoft.AspNetCore.Authorization;

namespace BoxDrop.Controllers
{
    public class FilesController : Controller
    {
        private readonly Context _context;
        private readonly IHostingEnvironment _appEnvironment;

        public FilesController(Context context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }
        /*
        public async Task<IActionResult> Info(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var file = await _context.Files
                .FirstOrDefaultAsync(m => m.Url == id);
            _context.Entry(file).Reference(s => s.User).Load();
            if (file == null)
            {
                return NotFound();
            }

            return View(file);
        }

        // GET: Files/GetFile/5
        public IActionResult Get(string id)
        {
            var url = id;
            if (url == null)
            {
                return NotFound();
            }

            File file = _context.Files
                .FirstOrDefault(m => m.Url == url);
            if (file == null)
            {
                return NotFound();
            }

            string path = "/Files/" + file.Url;
            string file_path = _appEnvironment.WebRootPath + path;

            string file_type = "application/octet-stream";
            string file_name = file.Name;
            return PhysicalFile(file_path, file_type, file_name);
        }

        // GET: Files/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var file = await _context.Files
                .FirstOrDefaultAsync(m => m.Url == id);
            if (file == null)
            {
                return NotFound();
            }

            return View(file);
        }

        // GET: Files/Add
        public IActionResult Add()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var url = API.UniqueKey.KeyGenerator.GetUniqueKey(10);
                string path = "/Files/" + url;

                using (var fileStream = new System.IO.FileStream(_appEnvironment.WebRootPath + path, System.IO.FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                User user = null;
                if (User.Identity.IsAuthenticated)
                {
                    var username = User.Identity.Name;
                    user = _context.Users
                        .FirstOrDefault(m => m.Login == username);
                }

                File file = new File { Name = uploadedFile.FileName, Path = path, Url = url, User = user };
                _context.Files.Add(file);
                _context.SaveChanges();
                return View(nameof(Info), file);
            }

            return View();
        }

        public async Task<IActionResult> Delete(string id)
        {
            var file = await _context.Files
                .FirstOrDefaultAsync(m => m.Url == id);

            string path = "/Files/" + file.Url;
            string file_path = _appEnvironment.WebRootPath + path;
            System.IO.File.Delete(file_path);

            _context.Files.Remove(file);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Add));
        }*/
    }
}
