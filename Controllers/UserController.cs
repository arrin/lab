﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using BoxDrop.Models;

namespace BoxDrop.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly Context _context;

        public UserController(Context context)
        {
            _context = context;
        }

        public IActionResult Files()
        {
            var username = User.Identity.Name;
            User user = _context.Users
                .FirstOrDefault(m => m.Login == username);
            _context.Entry(user).Collection(s => s.Files).Load();
            return View(user);
        }
    }
}