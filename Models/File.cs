﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BoxDrop.Models
{
    public class File
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public int SizeBytes { get; set; }

        public bool IsPublic { get; set; }

        public int OwnerId { get; set; }

        public User Owner { get; set; }

        public string Hash { get; set; }

        public override string ToString()
        {
            return $"File: {Name}";
        }
    }
}
