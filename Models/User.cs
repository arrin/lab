﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BoxDrop.Models
{
    public enum Permission { User, Moderator, Admin }

    public class User
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Permission Permissions { get; set; }

        public ICollection<File> Files { get; set; }

        public ICollection<Link> SharedLinks { get; set; }

        public override string ToString()
        {
            return $"{FirstName} ''{Login}'' {LastName}";
        }

        public User()
        {
            Files = new List<File>();
            Permissions = Permission.User;
        }
    }
}
